Communitree & Communiverse
::::::::::::::::::::::::::

PURPOSE:
The "Community Tree" displays most active Users and Nodes in a graphical form. When clicking on a user, lines are drawn to the nodes visited by the user. Node and user sizes depend on their popularity/activity.
The Tree consists of a Drupal services module (communiverse) that presents a services API to query user/nodes, and the javascript tool that pulls node/user information and graphically shows it (communitree).

CONTRIBUTIONS / COMMUNITY:
communitree/communiverse will be published as Opensource on drupal.org, the older versions are not published. License is GPL. Discussions: Forum XXX on labs.swisscom.com, or the drupal issue queues XX and XXX.

HISTORY:
This version (communitree/communiverse) is a port/rewrite of "ctree2" by undef (http://www.undef.ch/) for the Swisscom Labs and the communiverse_service module for Drupal 6.


:::::::::::: communiverse ::::::::::::::::::::::
To Do: summarise this module and the theme it depends on.

Config:
  Import the api endpoint definition file.
  http://SERVER/en/admin/structure/services

Testing:
http://SERVER/en/api/communiuser/3
http://SERVER/en/api/communiuser
http://SERVER/en/api/communiuser&limit=5

http://SERVER/en/api/communinode/10 
http://SERVER/en/api/communinode
http://SERVER/en/api/communinode?ntype=news
http://SERVER/en/api/communinode?ntype=app
http://SERVER/en/api/communinode?ntype=idea
http://SERVER/en/api/communinode?ntype=trialgroup 

Javascript front end: see communitree module
http://SERVER/sites/all/modules/communitree/index.html


